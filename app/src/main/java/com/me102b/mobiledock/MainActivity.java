package com.me102b.mobiledock;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.util.Date;
import java.util.List;
import java.util.Set;

/* Reference: Cone Finder - https://docs.google.com/document/d/1jkJJxI4b7J5IFasyTD4sB3HpCkvrs1R8zv8ZBdNt5Aw */

public class MainActivity extends Activity implements View.OnTouchListener, CameraBridgeViewBase.CvCameraViewListener2 {

    private CameraBridgeViewBase mOpenCvCameraView;
    private TextView mLeftRightLocationTextView, mTopBottomLocationTextView, mSizePercentageTextView;
    private Button mConnectButton;
    private Button mCommandButton;

    private ColorBlobDetector mDetector;
    private Scalar CONTOUR_COLOR = new Scalar(0, 0, 255, 255);
    private Mat mRgba;
    private CallReceiver callReceiver = new CallReceiver();

    private int mRobotStatus = STATUS_DOCKED;
    private static final int STATUS_DOCKED = 0;
    private static final int STATUS_GOING_TO_OWNER = 1;
    private static final int STATUS_ARRIVED_AT_OWNER = 2;
    private static final int STATUS_RETURNING_TO_DOCK = 3;

    /** Target color. Red hue of 0 - 10, full saturation and value. */
    private static final int TARGET_COLOR_HUE = 5;
    private static final int TARGET_COLOR_SATURATION = 255;
    private static final int TARGET_COLOR_VALUE = 255;

    /** Range of acceptable colors. (change as needed) */
    private static final int TARGET_COLOR_HUE_RANGE = 5;
    private static final int TARGET_COLOR_SATURATION_RANGE = 150;
    private static final int TARGET_COLOR_VALUE_RANGE = 180;

    /** Minimum size needed to consider the target as valid. (change as needed) */
    private static final double MIN_SIZE_PERCENTAGE = 0.0005;

    /** Screen size variables. */
    private double mCameraViewWidth;
    private double mCameraViewHeight;
    private double mCameraViewArea;

    // Message types sent from the BluetoothReadService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_DISCONNECTED = 6;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static final String CONNECTION_STATUS = "connection_status";

    private int mIncomingEoL_0D = 0x0D;
    private int mIncomingEoL_0A = 0x0A;
    private int mOutgoingEoL_0D = 0x0D;
    private int mOutgoingEoL_0A = 0x0A;

    private BluetoothAdapter mBluetoothAdapter = null;
    private static BluetoothSerialService mSerialService = null;
    private long mLastSentTime = System.currentTimeMillis(); // To ensure we don't overload Arduino BT buffer
    private long mSilenceUntilTime = 0; // Don't send any new commands until after specified time
    private static final long ROBOT_TURN_DELAY = 500; // millis
    private static final long BLUETOOTH_SEND_DELAY = 70; // millis

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setMaxFrameSize(400, 400);
        mOpenCvCameraView.setOnTouchListener(MainActivity.this);

        mLeftRightLocationTextView = (TextView)findViewById(R.id.left_right_location_value);
        mTopBottomLocationTextView = (TextView)findViewById(R.id.top_bottom_location_value);
        mSizePercentageTextView = (TextView)findViewById(R.id.size_percentage_value);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        if (mBluetoothAdapter == null) {
//            finishDialogNoBluetooth();
//            return;
//        }

        mSerialService = new BluetoothSerialService(this, mHandlerBT);
        connectBluetooth();

        IntentFilter intent = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(callReceiver, intent);

        mConnectButton = (Button) findViewById(R.id.connect_button);
        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connectBluetooth();
            }
        });

        mCommandButton = (Button) findViewById(R.id.command_button);
        mCommandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button) view;

                switch (mRobotStatus) {
                    // Docked -> Go to Owner
                    case STATUS_DOCKED:
                        goToOwner();
                        break;

                    // Go to Owner -> Arrived
                    case STATUS_GOING_TO_OWNER:
                        completeArrivalAtOwner();
                        break;

                    // Arrived -> Return to Station
                    case STATUS_ARRIVED_AT_OWNER:
                        if (!isPhoneCharging()) {
                            Toast.makeText(MainActivity.this, "Please plug your phone into the charging dock.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        returnToStation();
                        break;

                    // Return to Station -> Docked
                    case STATUS_RETURNING_TO_DOCK:
                        completeDocking();
                        break;
                }
            }
        });
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public boolean onTouch(View v, MotionEvent event) {
        int cols = mRgba.cols();
        int rows = mRgba.rows();

        int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2;
        int yOffset = (mOpenCvCameraView.getHeight() - rows) / 2;

        int x = (int)event.getX() - xOffset;
        int y = (int)event.getY() - yOffset;

        Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");

        if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;

        Rect touchedRect = new Rect();

        touchedRect.x = (x>4) ? x-4 : 0;
        touchedRect.y = (y>4) ? y-4 : 0;

        touchedRect.width = (x+4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y+4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);

        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region
        Scalar blobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width*touchedRect.height;
        for (int i = 0; i < blobColorHsv.val.length; i++)
            blobColorHsv.val[i] /= pointCount;

        mDetector.setHsvColor(blobColorHsv);

        touchedRegionRgba.release();
        touchedRegionHsv.release();

        return false; // don't need subsequent touch events
    }

    public void onImageRecComplete(boolean targetFound, double leftRightLocation, double topBottomLocation, double sizePercentage) {
        if (targetFound) {
            mLeftRightLocationTextView.setText(String.format("%.3f", leftRightLocation));
            mTopBottomLocationTextView.setText(String.format("%.3f", topBottomLocation));
            mSizePercentageTextView.setText(String.format("%.5f", sizePercentage));

            if (mRobotStatus != STATUS_DOCKED) {
                // Positive angle if object is left of phone, negative if right
                sendData((int) (leftRightLocation * 30), false);
            }
        }
    }

    private void sendData(int angle, boolean isCriticalMessage) {
        long currentTime = System.currentTimeMillis();
        if (isCriticalMessage || (currentTime > mSilenceUntilTime && currentTime - mLastSentTime > BLUETOOTH_SEND_DELAY)) {
            Integer angleInt = angle;
            send(new byte[]{angleInt.byteValue()});
            mLastSentTime = currentTime;
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver);
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mDetector = new ColorBlobDetector();

        // Setup the range of values around the color to accept.
        Scalar colorRangeHsv = new Scalar(255);
        colorRangeHsv.val[0] = TARGET_COLOR_HUE_RANGE;
        colorRangeHsv.val[1] = TARGET_COLOR_SATURATION_RANGE;
        colorRangeHsv.val[2] = TARGET_COLOR_VALUE_RANGE;
        mDetector.setColorRadius(colorRangeHsv);

        Scalar targetColorHsv = new Scalar(255);
        targetColorHsv.val[0] = TARGET_COLOR_HUE;
        targetColorHsv.val[1] = TARGET_COLOR_SATURATION;
        targetColorHsv.val[2] = TARGET_COLOR_VALUE;
        mDetector.setHsvColor(targetColorHsv);

        // Record the screen size constants
        mCameraViewWidth = (double)width;
        mCameraViewHeight = (double)height;
        mCameraViewArea = mCameraViewWidth * mCameraViewHeight;
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mDetector.process(mRgba);
        List<MatOfPoint> contours = mDetector.getContours();
        Imgproc.drawContours(mRgba, contours, -1, CONTOUR_COLOR);

        // Find the center of the cone.
        double[] coneResult = new double[3];
        final boolean targetFound = findTarget(contours, MIN_SIZE_PERCENTAGE, coneResult);
        final double leftRightLocation = coneResult[0]; // -1 for left ...  1 for right
        final double topBottomLocation = coneResult[1]; // 1 for top ... 0 for bottom
        final double sizePercentage = coneResult[2];
        if (targetFound) {
            // Draw a circle on the screen at the center.
            double coneCenterX = topBottomLocation * mCameraViewWidth;
            double coneCenterY = (leftRightLocation + 1.0) / 2.0 * mCameraViewHeight;
            // Core.circle(rgba, new Point(coneCenterX, coneCenterY), 5, CONTOUR_COLOR, -1);
        }
        runOnUiThread(new Runnable() {
            public void run() {
                onImageRecComplete(targetFound, leftRightLocation, topBottomLocation, sizePercentage);
            }
        });

        return mRgba;
    }

    /**
     * Performs the math to find the leftRightLocation, topBottomLocation, and sizePercentage values.
     *
     * @param contours List of matrices containing points that match the target color.
     * @param minSizePercentage Minimum size percentage needed to call a blob a match. 0.005 would be 0.5%
     * @param targetResult Array that will be populated with the results of this math.
     * @return True if a target is found, False if no target is found.
     */
    private boolean findTarget(List<MatOfPoint> contours, double minSizePercentage, double[] targetResult) {
        // Step #0: Determine if any contour regions were found that match the target color criteria.
        if (contours.size() == 0) {
            return false; // No contours found.
        }

        // Step #1: Use only the largest contour. Other contours (potential other cones) will be ignored.
        MatOfPoint largestContour = contours.get(0);
        double largestArea = Imgproc.contourArea(largestContour);
        for (int i = 1; i < contours.size(); ++i) {
            MatOfPoint currentContour = contours.get(0);
            double currentArea = Imgproc.contourArea(currentContour);
            if (currentArea > largestArea) {
                largestArea = currentArea;
                largestContour = currentContour;
            }
        }

        // Step #2: Determine if this target meets the size requirement.
        double sizePercentage = largestArea / mCameraViewArea;
        if (sizePercentage < minSizePercentage) {
            return false; // No cone found meeting the size requirement.
        }

        // Step #3: Calculate the center of the blob.
        Moments moments = Imgproc.moments(largestContour, false);
        double aveX = moments.get_m10() / moments.get_m00();
        double aveY = moments.get_m01() / moments.get_m00();

        // Step #4: Convert the X and Y values into leftRight and topBottom values.
        // X is 0 on the left (which is really the bottom) divide by width to scale the topBottomLocation
        // Y is 0 on the top of the view (object is left of the robot) divide by height to scale
        double leftRightLocation = aveY / (mCameraViewHeight / 2.0) - 1.0;
        double topBottomLocation = aveX / mCameraViewWidth;

        // Step #5: Populate the results array.
        targetResult[0] = leftRightLocation;
        targetResult[1] = topBottomLocation;
        targetResult[2] = sizePercentage;
        return true;
    }

    private byte[] handleEndOfLineChars( int outgoingEoL ) {
        byte[] out;

        if ( outgoingEoL == 0x0D0A ) {
            out = new byte[2];
            out[0] = 0x0D;
            out[1] = 0x0A;
        }
        else {
            if ( outgoingEoL == 0x00 ) {
                out = new byte[0];
            }
            else {
                out = new byte[1];
                out[0] = (byte)outgoingEoL;
            }
        }

        return out;
    }

    public void send(byte[] out) {

        if ( out.length == 1 ) {

            if ( out[0] == 0x0D ) {
                out = handleEndOfLineChars( mOutgoingEoL_0D );
            }
            else {
                if ( out[0] == 0x0A ) {
                    out = handleEndOfLineChars( mOutgoingEoL_0A );
                }
            }
        }

        if ( out.length > 0 ) {
            mSerialService.write( out );
        }
    }

    private void connectBluetooth() {
        // Get a set of currently paired devices
        // Hack: Set up phone to be paired with only our Bluetooth adapter
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        for (BluetoothDevice device : pairedDevices) {
            if ("HC-05".equals(device.getName())) {
                mSerialService.connect(device);
            }
        }
    }

    private boolean isPhoneCharging() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent mBatteryStatus = registerReceiver(null, ifilter);
        int plugged = mBatteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        return plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;
    }

    private void goToOwner() {
        mRobotStatus = STATUS_GOING_TO_OWNER;
        mCommandButton.setAlpha(0f);

        sendData(-128, true);
        mSilenceUntilTime = System.currentTimeMillis() + ROBOT_TURN_DELAY;
    }

    private void completeArrivalAtOwner() {
        mRobotStatus = STATUS_ARRIVED_AT_OWNER;
        mCommandButton.setText(R.string.return_to_station);
        mCommandButton.setAlpha(1f);
    }

    private void returnToStation() {
        mRobotStatus = STATUS_RETURNING_TO_DOCK;
        mCommandButton.setAlpha(0f);

        sendData(127, true); // Tell robot to return to station
        mSilenceUntilTime = System.currentTimeMillis() + ROBOT_TURN_DELAY;
    }

    private void completeDocking() {
        mRobotStatus = STATUS_DOCKED;
        mCommandButton.setText(R.string.go_to_owner);
        mCommandButton.setAlpha(1f);
    }

    // The Handler that gets information back from the BluetoothService
    private final Handler mHandlerBT = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_WRITE:
                    //if (mLocalEcho) {
                        byte[] writeBuf = (byte[]) msg.obj;
                        //mEmulatorView.write(writeBuf, msg.arg1);
                        Log.i(TAG, "Write: " + byteArrToString(writeBuf));
                    //}

                    break;

                case MESSAGE_READ:
                    String readMessage = (String) msg.obj;
                    if ("a".equals(readMessage)) {
                        completeArrivalAtOwner();
                    }
                    else if ("d".equals(readMessage)) {
                        completeDocking();
                    }
                    Log.i(TAG, "Received: " + readMessage);
                    break;

                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    String deviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to " + deviceName, Toast.LENGTH_SHORT).show();
                    mConnectButton.setVisibility(View.GONE);
                    mCommandButton.setVisibility(View.VISIBLE);
                    completeDocking(); // Reset to docked state
                    break;

                case MESSAGE_DISCONNECTED:
                    mConnectButton.setVisibility(View.VISIBLE);
                    mCommandButton.setVisibility(View.GONE);
                    break;

//                case MESSAGE_TOAST:
//                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
//                    break;
            }
        }
    };

    private String byteArrToString(byte[] arr) {
        String s = "";
        for (byte b : arr) {
            s += b + ", ";
        }
        return s;
    }

    private String byteLineToString(Message msg) {
        byte[] readBuf = (byte[]) msg.obj;
        String strIncom = new String(readBuf, 0, msg.arg1);                 // create string from bytes array
        StringBuilder sb = new StringBuilder();
        sb.append(strIncom);                                                // append string
        int endOfLineIndex = sb.indexOf("\r\n");                            // determine the end-of-line
        if (endOfLineIndex > 0) {                                            // if end-of-line,
            String sbprint = sb.substring(0, endOfLineIndex);               // extract string
            sb.delete(0, sb.length());                                      // and clear
            return sbprint;
        }
        else {
            return null;
        }
    }

    // Gets OpenCV native library loaded before Activity instantiation
    static {
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
    }

    private class CallReceiver extends PhonecallReceiver {

        @Override
        protected void onIncomingCallStarted(Context ctx, String number, Date start) {
            if (mRobotStatus == STATUS_DOCKED) {
                goToOwner();
            }
        }
    }

}